npm init -y
npm i mithril
npm i bootstrap@4.0.0-beta.2 # beta.3 doesn't work properly with autoprefixer + postcss 6
npm i -D webpack \
         webpack-merge webpack-dev-server uglifyjs-webpack-plugin \
         clean-webpack-plugin html-webpack-plugin \
         style-loader css-loader extract-text-webpack-plugin postcss-loader postcss-safe-parser cssnano autoprefixer \
         babel-loader babel-core babel-preset-env