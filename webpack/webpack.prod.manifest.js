const merge = require('webpack-merge');
const webpack = require('webpack');
const prod = require('./webpack.prod.js');

// vendor is just a convention, it can be named as anything
module.exports = merge(prod, {
  entry: {
    vendor: ['mithril', 'bootstrap']
  },
  plugins: [
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    })
  ]
});