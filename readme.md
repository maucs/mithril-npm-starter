# Mithril Starter Pack

I am not a Javascript developer, and will never be one.
Convoluted ecosystem, things moves too fast

I mean, look at the amount of config files located here

## Recipe

Framework is Mithril + Bootstrap (only to show how to concat Javascript files and add CSS files, use your own CSS framework)

Build tool is Webpack + whatever plugins required

## Init

Ignore the **package.json** file, generate a new one for your own project

```bash init.sh```

## Excluding vendors

If you want to use manifest, comment the external directive in the file **webpack.prod.js**

## Manifest

Without manifest, modifying the source code changes the vendor's hash

The manifest configuration depends heavily on production config. If the vendor files is ignored in production build,
it's ignored in manifest build

## Webpack

Webpack configuration is divided. Refer to the **package.json**'s script directive for more details

## Related Documentations

- https://getbootstrap.com/docs/4.0/getting-started/webpack/