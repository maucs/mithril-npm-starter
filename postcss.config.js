module.exports = {
  parser: 'postcss-safe-parser',
  plugins: {
    'cssnano': {
      preset: 'default'
    },
    'autoprefixer': {}
  }
}
